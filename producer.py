import sys
from confluent_kafka import Producer


class TweetProducer(Producer):

    def __init__(self, **config):
        super(TweetProducer, self).__init__({
            'bootstrap.servers': config['broker'],
            'acks': 'all',
            'max.in.flight.requests.per.connection': 5,
            "retries": 10000000,
         })


    def delivery_callback(self, error, msg):
        if error:
            sys.stderr.write('%% Message failed delivery: {}'.format(error))
        else:
            sys.stderr.write('%% Message delivered to {} [{}] @ {}'.format(msg.topic(), msg.partition(), msg.offset()))

    def send_data(self, status, topic, callback_def):
        try:
            self.poll(0)
            self.produce(topic, status, callback=callback_def)
        except KeyboardInterrupt:
            self.flush()
        except BufferError:
            sys.sterr.write('%% Local producer queue is full ({} messages awaiting delivery): try again\n'.format(len(self)))


