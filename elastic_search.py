import elasticsearch
import json


class CustomSearch(elasticsearch.Elasticsearch):
    
    def __init__(self, host, username, password):
        super(CustomSearch, self).__init__(host, http_auth=(username, password))

    def create_index(self, index_name):
        self.indices.create(index=index_name, ignore=400)

    def add_document(self, index_name, doc_type, body, document_id):
        return self.index(index=index_name, doc_type=doc_type, body=body, id=document_id)
