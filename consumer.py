from confluent_kafka import Consumer, KafkaException
import sys
import argparse
import json
import time

class TweetConsumer(Consumer):

    def __init__(self, **config):
        super(TweetConsumer, self).__init__({
            'bootstrap.servers': config['broker'],
            'group.id': config['group'],
            'auto.offset.reset': config['offset'],
            'session.timeout.ms': int(config['timeout']),
            'enable.partition.eof': 'false'
        })

    def print_assignment(self, partitions):
        print('Assignment: ', partitions)

    def subscribe_to_topics(self, topics):
        self.subscribe(topics)

    def read_data(self, index_name, doc_type, add_record):
        try:
            while(True):
                msg = self.poll(timeout=1)
                if msg is None:
                    continue
                if msg.error():
                    raise KafkaException(msg.error())
                else:
                    try:
                        sys.stderr.write('%% {} [{}] at offset {} with key {}\n'.format(msg.topic(), msg.partition(), msg.offset(), str(msg.key())))
                        add_record(index_name, doc_type, msg.value(), json.loads(msg.value())['id_str'])
                        print(json.loads(msg.value())['id_str'])
                    except Exception as e:
                        print('got an error, moving on..{}'.format(e))
                        continue
        except KeyboardInterrupt:
            sys.stderr.write('%% Aborted by user\n')
        finally:
            self.close()






if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--broker', help='Kafka server')
    parser.add_argument('--group', help='Consumer group')
    parser.add_argument('--offset', help='Topic offset')
    parser.add_argument('--timeout', help='Poll\'s timeout')
    parser.add_argument('--topics', nargs='+', help='List of topics to subscribe')

    args = parser.parse_args()

    consumer = TweetConsumer(broker=args.broker, group=args.group,
                            offset=args.offset, timeout=args.timeout)
    
    consumer.subscribe_to_topics(args.topics)
    consumer.read_data()
