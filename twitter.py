import tweepy
import time
import json

class TweeterStream(tweepy.StreamListener):

    def __init__(self, topic, twitter_producer):
        super(TweeterStream, self).__init__()
        self.topic = topic
        self.twitter_producer = twitter_producer

    def on_status(self, status):
        self.twitter_producer.send_data(json.dumps(status._json), self.topic, self.twitter_producer.delivery_callback)
        # print(type(status._json))

    def get_auth(self, consumer_key, consumer_secret, access_token, access_token_secret):
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        return auth

    def get_api(self, auth):
        return tweepy.API(auth)

    def get_stream(self, api, listener):
        return tweepy.Stream(auth=api.auth, listener=listener)
