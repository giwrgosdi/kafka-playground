from producer import TweetProducer
from twitter import TweeterStream
import argparse
import sys



if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Kafka Producer')
    parser.add_argument('--broker', help='provide Kafka server', required=True)
    parser.add_argument('--topic', help='provide Kafka topic', required=True)
    parser.add_argument('--term', help='provide the temr to search for', required=True)
    parser.add_argument('--consumer-key', help='provide the consumer key for twitter', required=True)
    parser.add_argument('--consumer-secret', help='provide the consumer secret for twitter', required=True)
    parser.add_argument('--access-token', help='provide the access token for twitter', required=True)
    parser.add_argument('--access-token-secret', help='provide the access token secret for twitter', required=True)
    args = parser.parse_args()



    twitter_producer = TweetProducer(broker=args.broker)
    

    twitter_listener = TweeterStream(args.topic, twitter_producer)
    auth = twitter_listener.get_auth(args.consumer_key, args.consumer_secret, args.access_token, args.access_token_secret)
    api = twitter_listener.get_api(auth)
    twitter_stream = twitter_listener.get_stream(api, twitter_listener)
    twitter_stream.filter(track=[args.term])
