from elastic_search import CustomSearch
from consumer import TweetConsumer
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Elasticsearch Kafka Consumer')
    parser.add_argument('--host', help='Elasticsearch server', required=True)
    parser.add_argument('--username', help='Elasticsearch username', required=True)
    parser.add_argument('--password', help='Elasticsearch password', required=True)
    parser.add_argument('--index-name', help='Name of the index to be created', required=True)
    parser.add_argument('--broker', help='Kafka server', required=True)
    parser.add_argument('--group', help='Consumer group', required=True)
    parser.add_argument('--offset', help='Topic offset', required=True)
    parser.add_argument('--timeout', help='Poll\'s timeout', required=True)
    parser.add_argument('--topics', nargs='+', help='List of topics to subscribe', required=True)
    args = parser.parse_args()
    

    es = CustomSearch(args.host, args.username, args.password)

    es.create_index(args.index_name)
    
    consumer = TweetConsumer(broker=args.broker, group=args.group,
                            offset=args.offset, timeout=args.timeout)
    
    consumer.subscribe_to_topics(args.topics)
    consumer.read_data(args.index_name, 'json', es.add_document)
